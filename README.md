# Undone Puzzler

The goal of this project was to create a puzzle game that used the Commnd Pattern to support the 'Undo' Functionality.

I wanted to experiment with making the concept of 'Undo' critical to gameplay. I found the best way to do this was a puzzle structure where undo only changes your actions, therefore, moving objects in the world will respond to your changes including the 'undo'. Then, level design was important to make this mechanic engangine in a way that undo was critical

The project was implemented in 1 week.

# Project specifications

This project was created using Unreal Engine 5.0
