// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class CommandAction;

/**
 * 
 */
class MODULE3_API CommandActionList
{
public:
	CommandActionList();
	~CommandActionList();

	CommandAction* Action;
	CommandActionList* NextAction;
	CommandActionList* LastAction;
};
