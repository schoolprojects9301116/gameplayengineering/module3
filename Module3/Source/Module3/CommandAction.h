// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class MODULE3_API CommandAction
{
public:
	CommandAction();
	virtual ~CommandAction();

	virtual void DoAction() = 0;
	virtual void UndoAction() = 0;
};
