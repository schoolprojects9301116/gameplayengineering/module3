// Fill out your copyright notice in the Description page of Project Settings.


#include "MouseBasedPlayerController.h"
#include "PuzzleGameMode.h"

AMouseBasedPlayerController::AMouseBasedPlayerController()
{
	bEnableMouseOverEvents = true;
	bShowMouseCursor = true;
	bEnableClickEvents = true;
}

void AMouseBasedPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("UndoAction", EInputEvent::IE_Pressed, this, &AMouseBasedPlayerController::UndoAction);
	InputComponent->BindAction("RedoAction", EInputEvent::IE_Pressed, this, &AMouseBasedPlayerController::RedoAction);
}

void AMouseBasedPlayerController::UndoAction()
{
	auto puzzleGameMode = Cast<APuzzleGameMode>(GetWorld()->GetAuthGameMode());
	if (puzzleGameMode)
	{
		puzzleGameMode->UndoAction();//CommandActionManager.UndoAction();
	}
}

void AMouseBasedPlayerController::RedoAction()
{
	auto puzzleGameMode = Cast<APuzzleGameMode>(GetWorld()->GetAuthGameMode());
	if (puzzleGameMode)
	{
		puzzleGameMode->RedoAction();// CommandActionManager.RedoAction();
	}
}
