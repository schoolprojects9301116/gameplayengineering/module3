// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Monster.generated.h"

class APuzzleGridCenter;

UCLASS()
class MODULE3_API AMonster : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMonster();

	void MakeMove(APuzzleGridCenter* target);
	APuzzleGridCenter* CurrentGridPos;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* _monsterMesh;

	UPROPERTY(EditInstanceOnly)
	APuzzleGridCenter* _startingPoint;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
