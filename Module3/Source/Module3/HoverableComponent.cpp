// Fill out your copyright notice in the Description page of Project Settings.


#include "HoverableComponent.h"

// Sets default values for this component's properties
UHoverableComponent::UHoverableComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	_normalMaterial = CreateDefaultSubobject<UMaterial>(TEXT("OriginalMaterial"));
	_hoverMaterial = CreateDefaultSubobject<UMaterial>(TEXT("HoverMaterial"));

	//_referenceMesh = Get
	//_referenceMesh
	//CustomMeshComponent->OnBeginCursorOver.AddDynamic(this, &AMyActor::CustomOnBeginMouseOver);
	// ...
}


// Called when the game starts
void UHoverableComponent::BeginPlay()
{
	Super::BeginPlay();
	// ...
	_referenceMesh = Cast<UStaticMeshComponent>(GetOwner()->GetComponentByClass(UStaticMeshComponent::StaticClass()));
	_referenceMesh->SetMaterial(0, _normalMaterial);
	EnableHover();
	/*_referenceMesh->OnBeginCursorOver.AddDynamic(this, &UHoverableComponent::MouseBeginHover);
	_referenceMesh->OnEndCursorOver.AddDynamic(this, &UHoverableComponent::MouseEndHover);*/
}

void UHoverableComponent::MouseBeginHover(UPrimitiveComponent* TouchedComponent)
{
	auto hoveredOn = Cast<UStaticMeshComponent>(TouchedComponent);
	if (hoveredOn != nullptr && hoveredOn == _referenceMesh)
	{
		//_normalMaterial = hoveredOn->GetMaterial(0)->GetMaterial();
		hoveredOn->SetMaterial(0, _hoverMaterial);
	}
}

void UHoverableComponent::MouseEndHover(UPrimitiveComponent * TouchedComponent)
{
	auto hoveredOn = Cast<UStaticMeshComponent>(TouchedComponent);
	if(hoveredOn != nullptr && hoveredOn == _referenceMesh)
	{
		hoveredOn->SetMaterial(0, _normalMaterial);
	}
}

void UHoverableComponent::DisableHover()
{
	_referenceMesh->OnBeginCursorOver.RemoveDynamic(this, &UHoverableComponent::MouseBeginHover);
	_referenceMesh->OnEndCursorOver.RemoveDynamic(this, &UHoverableComponent::MouseEndHover);
	_referenceMesh->SetMaterial(0, _normalMaterial);
}

void UHoverableComponent::EnableHover()
{
	_referenceMesh->OnBeginCursorOver.AddDynamic(this, &UHoverableComponent::MouseBeginHover);
	_referenceMesh->OnEndCursorOver.AddDynamic(this, &UHoverableComponent::MouseEndHover);
}

