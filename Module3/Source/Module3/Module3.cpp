// Copyright Epic Games, Inc. All Rights Reserved.

#include "Module3.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Module3, "Module3" );
