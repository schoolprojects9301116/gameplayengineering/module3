// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PuzzleGrid.generated.h"

UCLASS()
class MODULE3_API APuzzleGrid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APuzzleGrid();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	USceneComponent* _rootSceneComponent;

	UPROPERTY(EditAnywhere)
	FVector2D _GridDimensions;

	UPROPERTY(EditAnywhere)
	float _GridSquaresOffset;

	UPROPERTY(EditAnywhere)
	float _GridSquareEdgeOffset;

	UPROPERTY(EditAnywhere)
	UStaticMesh* _EdgeMeshType;

	UPROPERTY(EditAnywhere)
	UStaticMesh* _CenterMeshType;

	void SpawnEdge(FVector gridSquareCenter, FRotator orientation, float xOffsetScale, float yOffsetScale);
};
