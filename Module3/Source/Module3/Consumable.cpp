// Fill out your copyright notice in the Description page of Project Settings.


#include "Consumable.h"
#include "Monster.h"
#include "PuzzleGameMode.h"

// Sets default values for this component's properties
UConsumable::UConsumable()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UConsumable::BeginPlay()
{
	Super::BeginPlay();

	_myMesh = Cast<UStaticMeshComponent>(GetOwner()->GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (_myMesh != nullptr)
	{
		_myMesh->OnComponentBeginOverlap.AddDynamic(this, &UConsumable::OnOverlap);
		_myMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		_myMesh->SetGenerateOverlapEvents(true);
		_myMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	}

	auto puzzleGame = Cast<APuzzleGameMode>(GetWorld()->GetAuthGameMode());
	if (puzzleGame != nullptr)
	{
		if (_isTarget)
		{
			puzzleGame->NumTargets++;
		}
		else
		{
			puzzleGame->Score++;
		}
		puzzleGame->Consumables.Add(this);
	}
	// ...
	
}

void UConsumable::OnOverlap(class UPrimitiveComponent* OverlappedComp
		, class AActor* OtherActor
		, class UPrimitiveComponent* OtherComp
		, int32 OtherBodyIndex
		, bool bFromSweep
	, const FHitResult& SweepResult)
{
	auto monster = Cast<AMonster>(OtherActor);
	if (monster != nullptr)
	{
		auto puzzleGame = Cast<APuzzleGameMode>(GetWorld()->GetAuthGameMode());
		if (puzzleGame != nullptr)
		{
			if (_isTarget)
			{
				puzzleGame->NumTargets--;
			}
			else
			{
				puzzleGame->Score--;
			}
			puzzleGame->Consumables.Remove(this);
		}
		GetOwner()->Destroy();
	}
}

// Called every frame
void UConsumable::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

