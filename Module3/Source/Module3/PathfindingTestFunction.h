// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PathfindingTestFunction.generated.h"

class APuzzleGridCenter;

UCLASS()
class MODULE3_API APathfindingTestFunction : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APathfindingTestFunction();

protected:

	UPROPERTY(EditAnywhere)
	float dTime;

	float currentDTime;

	UPROPERTY(EditInstanceOnly)
	APuzzleGridCenter * start;

	UPROPERTY(EditInstanceOnly)
	APuzzleGridCenter * end;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
