// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PuzzleGridEdge.generated.h"

class UHoverableComponent;

UCLASS()
class MODULE3_API APuzzleGridEdge : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APuzzleGridEdge();

	UStaticMeshComponent* GetFenceMeshComponent();
	UHoverableComponent* GetHoverableComponent();

	void PlaceFence();
	void RemoveFence();

	bool IsBlocked();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditInstanceOnly)
	bool StartWithWall = false;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* _rootMesh;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* _fenceMesh;

	UPROPERTY(EditAnywhere)
	UHoverableComponent * _hoverableComponent;

	bool _isBlocked;

	UFUNCTION()
	void OnEdgeClicked(UPrimitiveComponent* Target, FKey ButtonPressed);

};
