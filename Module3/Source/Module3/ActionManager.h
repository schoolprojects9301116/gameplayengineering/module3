// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class CommandAction;
class CommandActionList;
/**
 * 
 */
class MODULE3_API ActionManager
{
public:
	ActionManager();
	~ActionManager();

	void AddAction(CommandAction * action);
	bool UndoAction();
	bool RedoAction();
	bool CanRedo();

private:
	CommandActionList* _headAction;
	CommandActionList* _currentAction;
};
