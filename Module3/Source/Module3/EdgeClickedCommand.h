// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommandAction.h"

#include "CoreMinimal.h"

class APuzzleGridEdge;

/**
 * 
 */
class MODULE3_API EdgeClickedCommand : public CommandAction
{
public:
	EdgeClickedCommand(APuzzleGridEdge* gridEdge);
	~EdgeClickedCommand();

	virtual void DoAction() override;
	virtual void UndoAction() override;

private:
	APuzzleGridEdge* _gridEdge;
};
