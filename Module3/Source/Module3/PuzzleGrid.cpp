// Fill out your copyright notice in the Description page of Project Settings.


#include "PuzzleGrid.h"
#include "Engine/StaticMeshActor.h"

// Sets default values
APuzzleGrid::APuzzleGrid()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	_rootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
}

// Called when the game starts or when spawned
void APuzzleGrid::BeginPlay()
{
	Super::BeginPlay();

	auto world = GetWorld();
	FVector GridSpawnCenterPosition = GetActorLocation();
	FRotator GridOrientation = GetActorRotation();
	FRotator GridOrientationTangent = GridOrientation;
	GridOrientationTangent.Add(0, 90, 0);
	for (int x = 0; x < _GridDimensions.X; x++)
	{
		for (int y = 0; y < _GridDimensions.Y; y++)
		{
			FVector GridSectionCenter = GridSpawnCenterPosition + FVector(x * _GridSquaresOffset, y * _GridSquaresOffset, 0);
			AStaticMeshActor* centerActor = world->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass()
				, GridSectionCenter, GridOrientation);
			UStaticMeshComponent* centerMeshComponent = centerActor->GetStaticMeshComponent();
			if(centerMeshComponent)
			{
				centerMeshComponent->SetStaticMesh(_CenterMeshType);
			}
			SpawnEdge(GridSectionCenter, GridOrientation, -1, 0);
			SpawnEdge(GridSectionCenter, GridOrientationTangent, 0, -1);
			if (x == _GridDimensions.X - 1)
			{
				SpawnEdge(GridSectionCenter, GridOrientation, 1, 0);
			}
			if (y == _GridDimensions.Y - 1)
			{
				SpawnEdge(GridSectionCenter, GridOrientationTangent, 0, 1);
			}
		}
	}
}

void APuzzleGrid::SpawnEdge(FVector gridSquareCenter, FRotator orientation, float xOffsetScale, float yOffsetScale)
{
	FVector spawnPos = gridSquareCenter + FVector(xOffsetScale * _GridSquareEdgeOffset
		, yOffsetScale * _GridSquareEdgeOffset, 0);
	AStaticMeshActor* edgeActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass()
		, spawnPos, orientation);
	UStaticMeshComponent* edgeMeshComponent = edgeActor->GetStaticMeshComponent();
	if (edgeMeshComponent)
	{
		edgeMeshComponent->SetStaticMesh(_EdgeMeshType);
	}
}

