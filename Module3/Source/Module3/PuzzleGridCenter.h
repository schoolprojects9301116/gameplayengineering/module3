// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Math/IntVector.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PuzzleGridCenter.generated.h"

class APuzzleGridEdge;

UCLASS()
class MODULE3_API APuzzleGridCenter : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APuzzleGridCenter();

	UPROPERTY(EditInstanceOnly, Category="GridManagement|GridPos")
	int GridX;

	UPROPERTY(EditInstanceOnly, Category="GridManagement|GridPos")
	int GridY;

	std::tuple<int, int>GridPos;

	UPROPERTY(EditInstanceOnly, Category="GridManagement|CenterPieces")
	APuzzleGridCenter* NorthGridCenter;

	UPROPERTY(EditInstanceOnly, Category = "GridManagement|CenterPieces")
	APuzzleGridCenter* SouthGridCenter;

	UPROPERTY(EditInstanceOnly, Category = "GridManagement|CenterPieces")
	APuzzleGridCenter* EastGridCenter;

	UPROPERTY(EditInstanceOnly, Category = "GridManagement|CenterPieces")
	APuzzleGridCenter* WestGridCenter;

	UPROPERTY(EditInstanceOnly, Category="GridManagement|EdgePieces")
	APuzzleGridEdge* NorthEdge;

	UPROPERTY(EditInstanceOnly, Category = "GridManagement|EdgePieces")
	APuzzleGridEdge* SouthEdge;

	UPROPERTY(EditInstanceOnly, Category = "GridManagement|EdgePieces")
	APuzzleGridEdge* EastEdge;

	UPROPERTY(EditInstanceOnly, Category = "GridManagement|EdgePieces")
	APuzzleGridEdge* WestEdge;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* _meshComponent;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
