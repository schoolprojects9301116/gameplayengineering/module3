// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MouseBasedPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MODULE3_API AMouseBasedPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AMouseBasedPlayerController();
	virtual void SetupInputComponent() override;

protected:
	UFUNCTION()
	void UndoAction();

	UFUNCTION()
	void RedoAction();
};
