// Fill out your copyright notice in the Description page of Project Settings.


#include "CommandActionList.h"
#include "CommandAction.h"

CommandActionList::CommandActionList()
	: Action(nullptr), NextAction(nullptr), LastAction(nullptr)
{}

CommandActionList::~CommandActionList()
{
	if (NextAction != nullptr)
	{
		delete NextAction;
	}
	if (Action != nullptr)
	{
		delete Action;
	}
}
