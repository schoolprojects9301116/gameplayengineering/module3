// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Consumable.generated.h"


class APuzzleGridCenter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MODULE3_API UConsumable : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UConsumable();

	UPROPERTY(EditInstanceOnly)
	APuzzleGridCenter* GridCenter;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UStaticMeshComponent* _myMesh;

	UFUNCTION()
	void OnOverlap(UPrimitiveComponent* OverlappedComp
			, AActor* OtherActor
			, UPrimitiveComponent* OtherComp
			, int32 OtherBodyIndex
			, bool bFromSweep
		, const FHitResult& SweepResult);

	UPROPERTY(EditAnywhere)
	bool _isTarget;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
