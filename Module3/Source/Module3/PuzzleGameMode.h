// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ActionManager.h"

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PuzzleGameMode.generated.h"

class UConsumable;
class AMonster;

/**
 * 
 */
UCLASS()
class MODULE3_API APuzzleGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	APuzzleGameMode();

	ActionManager CommandActionManager;

	void AddAction(CommandAction* action);

	UFUNCTION(BlueprintCallable)
	void UndoAction();

	UFUNCTION(BlueprintCallable)
	void RedoAction();


	TArray<UConsumable*> Consumables;
	TArray<AMonster*> Monsters;

	UPROPERTY(BlueprintReadWrite)
	int Score = 0;
	int NumTargets = 0;


	virtual void Tick(float DeltaTime) override;

protected:
	UPROPERTY(EditAnywhere)
	float _updateInterval = 1.f;

	float _updateTimer = 0.f;

	virtual void StartPlay() override;

	UPROPERTY(BlueprintReadWrite)
	bool _levelComplete = false;

	void NotifyMonsters();
	int ManhattanDistance(std::tuple<int, int> a, std::tuple<int, int>b);
};
