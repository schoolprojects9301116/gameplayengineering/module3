// Fill out your copyright notice in the Description page of Project Settings.


#include "Monster.h"
#include "PathfindingAlgorithms.h"
#include "PuzzleGridCenter.h"
#include "PuzzleGameMode.h"

// Sets default values
AMonster::AMonster()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	_monsterMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MonsterMesh"));
	_monsterMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	_monsterMesh->SetGenerateOverlapEvents(true);
	_monsterMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

void AMonster::MakeMove(APuzzleGridCenter* target)
{
	/*auto gameMode = Cast<APuzzleGameMode>(GetWorld()->GetAuthGameMode());
	if (gameMode != nullptr)
	{
		gameMode->generator
	}*/
	/*auto _nextPoint = PathfindingAlgorithms::PuzzleGameAStar(CurrentGridPos, target);
	if (_nextPoint != nullptr)
	{
		FVector targetPoint = _nextPoint->GetActorLocation();
		targetPoint.Z = GetActorLocation().Z;
		SetActorLocation(targetPoint);
		CurrentGridPos = _nextPoint;
	}*/
	if (target != nullptr)
	{
		FVector targetPoint = target->GetActorLocation();
		targetPoint.Z = GetActorLocation().Z;
		SetActorLocation(targetPoint);
		CurrentGridPos = target;
	}
}

// Called when the game starts or when spawned
void AMonster::BeginPlay()
{
	Super::BeginPlay();
	CurrentGridPos = _startingPoint;
	auto gameMode = Cast<APuzzleGameMode>(GetWorld()->GetAuthGameMode());
	if (gameMode)
	{
		gameMode->Monsters.Add(this);
	}
}

// Called every frame
void AMonster::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

