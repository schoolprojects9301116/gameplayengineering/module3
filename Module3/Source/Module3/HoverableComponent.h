// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HoverableComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MODULE3_API UHoverableComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHoverableComponent();
	UFUNCTION()
	void MouseBeginHover(UPrimitiveComponent* TouchedComponent);
	
	UFUNCTION()
	void MouseEndHover(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	void DisableHover();

	UFUNCTION()
	void EnableHover();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(Transient)
	UStaticMeshComponent* _referenceMesh;

	UPROPERTY(EditAnywhere)
	UMaterial* _hoverMaterial;

	UPROPERTY(EditAnywhere)
	UMaterial* _normalMaterial;

};
