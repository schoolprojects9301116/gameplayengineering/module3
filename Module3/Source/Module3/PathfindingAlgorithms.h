// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

class APuzzleGridCenter;

namespace PathfindingAlgorithms
{
	TArray<APuzzleGridCenter*> PuzzleGameAStar(APuzzleGridCenter* start, APuzzleGridCenter* end);
}