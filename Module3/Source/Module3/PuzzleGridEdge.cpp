// Fill out your copyright notice in the Description page of Project Settings.


#include "PuzzleGridEdge.h"
#include "EdgeClickedCommand.h"
#include "HoverableComponent.h"
#include "PuzzleGameMode.h"

// Sets default values
APuzzleGridEdge::APuzzleGridEdge()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	_rootMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("RootMesh"));

	_fenceMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FenceMesh"));

	_hoverableComponent = CreateDefaultSubobject<UHoverableComponent>(TEXT("HoverableComponent"));

	SetRootComponent(_rootMesh);

	_fenceMesh->SetupAttachment(_rootMesh);
	
	_rootMesh->OnClicked.AddDynamic(this, &APuzzleGridEdge::OnEdgeClicked);
}

// Called when the game starts or when spawned
void APuzzleGridEdge::BeginPlay()
{
	Super::BeginPlay();

	_isBlocked = false;
	//_fenceMesh->Deactivate();
	//_fenceMesh->SetHiddenInGame(true);
	_fenceMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	if (StartWithWall)
	{
		PlaceFence();
	}
	else
	{
		RemoveFence();
	}
}

void APuzzleGridEdge::OnEdgeClicked(UPrimitiveComponent* Target, FKey ButtonPressed)
{
	CommandAction* command = new EdgeClickedCommand(this);
	APuzzleGameMode* gameMode = Cast<APuzzleGameMode>(GetWorld()->GetAuthGameMode());
	if (gameMode != nullptr)
	{
		gameMode->AddAction(command);//CommandActionManager.AddAction(command);
	}
}

UStaticMeshComponent* APuzzleGridEdge::GetFenceMeshComponent()
{
	return _fenceMesh;
}

UHoverableComponent* APuzzleGridEdge::GetHoverableComponent()
{
	return _hoverableComponent;
}

void APuzzleGridEdge::PlaceFence()
{
	//_hoverableComponent->Deactivate();
	_hoverableComponent->DisableHover();
	_fenceMesh->SetHiddenInGame(false);
	_fenceMesh->SetVisibility(true);
	_fenceMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	_isBlocked = true;
}

void APuzzleGridEdge::RemoveFence()
{
	//_hoverableComponent->Activate();
	_hoverableComponent->EnableHover();
	_fenceMesh->SetHiddenInGame(true);
	_fenceMesh->SetVisibility(false);
	_fenceMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	_isBlocked = false;
}

bool APuzzleGridEdge::IsBlocked()
{
	return _isBlocked;
}
