// Fill out your copyright notice in the Description page of Project Settings.


#include "EdgeClickedCommand.h"
#include "PuzzleGridEdge.h"

EdgeClickedCommand::EdgeClickedCommand(APuzzleGridEdge* gridEdge)
	: _gridEdge(gridEdge)
{}

EdgeClickedCommand::~EdgeClickedCommand()
{}

void EdgeClickedCommand::DoAction()
{
	_gridEdge->PlaceFence();
}

void EdgeClickedCommand::UndoAction()
{
	_gridEdge->RemoveFence();
}
