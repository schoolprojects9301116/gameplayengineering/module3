// Fill out your copyright notice in the Description page of Project Settings.


#include "ActionManager.h"
#include "CommandAction.h"
#include "CommandActionList.h"

ActionManager::ActionManager() : _headAction(nullptr), _currentAction(nullptr)
{}

ActionManager::~ActionManager()
{
	if (_headAction != nullptr)
	{
		delete _headAction;
	}
}

void ActionManager::AddAction(CommandAction* action)
{
	auto nextAction = new CommandActionList();
	nextAction->Action = action;
	if (_headAction == nullptr)
	{
		_headAction = nextAction;
	}
	else if (_currentAction == nullptr)
	{
		delete _headAction;
		_headAction = nextAction;
	}
	else
	{
		if (_currentAction->NextAction != nullptr)
		{
			delete _currentAction->NextAction;
		}
		_currentAction->NextAction = nextAction;
		nextAction->LastAction = _currentAction;
	}
	_currentAction = nextAction;
	nextAction->Action->DoAction();
}

bool ActionManager::UndoAction()
{
	if (_currentAction != nullptr)
	{
		_currentAction->Action->UndoAction();
		_currentAction = _currentAction->LastAction;
		return true;
	}
	return false;
}

bool ActionManager::RedoAction()
{
	if (CanRedo())
	{
		if (_currentAction == nullptr)
		{
			_headAction->Action->DoAction();
			_currentAction = _headAction;
		}
		else
		{
			_currentAction = _currentAction->NextAction;
			_currentAction->Action->DoAction();
		}
		return true;
	}
	return false;
}

bool ActionManager::CanRedo()
{
	if (_headAction == nullptr)
	{
		return false;
	}
	else if (_currentAction == nullptr)
	{
		return true;
	}
	else  if (_currentAction->NextAction == nullptr)
	{
		return false;
	}
	return true;
}