// Fill out your copyright notice in the Description page of Project Settings.


#include "PuzzleGridCenter.h"

// Sets default values
APuzzleGridCenter::APuzzleGridCenter()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	_meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	SetRootComponent(_meshComponent);
}

// Called when the game starts or when spawned
void APuzzleGridCenter::BeginPlay()
{
	Super::BeginPlay();
	GridPos = std::tuple<int, int>(GridX, GridY);
}

// Called every frame
void APuzzleGridCenter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

