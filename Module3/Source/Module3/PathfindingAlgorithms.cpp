// Fill out your copyright notice in the Description page of Project Settings.


#include "PathfindingAlgorithms.h"
#include "PuzzleGridCenter.h"
#include "PuzzleGridEdge.h"
#include "Math/IntVector.h"

#include <map>
#include <queue>

namespace PathfindingAlgorithms
{

struct PuzzleVisitedNode
{
	APuzzleGridCenter* Node;
	std::tuple<int, int> CameFrom;
	float TotalCost;
};

struct PuzzleSearchNode
{
	APuzzleGridCenter* Node;
	float estimateCost;
};

bool operator<(const PuzzleSearchNode& left, const PuzzleSearchNode& right)
{
	return right.estimateCost < left.estimateCost;
}

bool operator<(const std::tuple<int, int>& left, const std::tuple<int, int>& right)
{
	return (std::get<0>(left) * 1000 + std::get<1>(left)) < (std::get<0>(right) * 1000 + std::get<1>(right));
}

void CheckChildNode(std::map<std::tuple<int, int>, PuzzleVisitedNode>& visited
	, std::priority_queue<PuzzleSearchNode>& pq
	, PuzzleSearchNode& currentNode
	, APuzzleGridCenter* nextGrid
	, APuzzleGridCenter* end)
{
	if (nextGrid != nullptr)
	{
		auto nextPos = nextGrid->GridPos;
		auto currentPos = currentNode.Node->GridPos;
		float costSoFar = visited.at(currentPos).TotalCost;
		if (visited.find(nextPos) != visited.end())
		{
			if (visited.at(nextPos).TotalCost <= costSoFar + 1)
			{
				return;
			}
		}
		else
		{
			PuzzleVisitedNode visitNode{ nextGrid, currentPos, costSoFar + 1 };
			visited.insert_or_assign(nextPos, visitNode);
			float heuristicGuess = abs(end->GridX - nextGrid->GridX) + abs(end->GridY - nextGrid->GridY);
			PuzzleSearchNode searchNode{ nextGrid, heuristicGuess };
			pq.push(searchNode);
		}
	}
}

TArray<APuzzleGridCenter*> PuzzleGameAStar(APuzzleGridCenter* start, APuzzleGridCenter* end)
{
	TArray<APuzzleGridCenter*> path;
	std::tuple<int, int> a(0, 1);
	std::tuple<int, int> b(1, 0);
	bool alessb = a < b;
	std::map<std::tuple<int, int>, PuzzleVisitedNode> visited;
	std::priority_queue<PuzzleSearchNode> pq;
	PuzzleSearchNode startSearchNode{ start, 0 };
	pq.push(startSearchNode);

	PuzzleVisitedNode startVisitedNode{ start, start->GridPos, 0 };
	visited.insert_or_assign(start->GridPos, startVisitedNode);

	bool foundEnd = false;
	PuzzleSearchNode currentNode;
	while (pq.size() > 0)
	{
		currentNode = pq.top();
		pq.pop();
		if (currentNode.Node == end)
		{
			foundEnd = true;
			break;
		}
		if (!currentNode.Node->NorthEdge->IsBlocked())
		{
			CheckChildNode(visited, pq, currentNode, currentNode.Node->NorthGridCenter, end);
		}
		if (!currentNode.Node->SouthEdge->IsBlocked())
		{
			CheckChildNode(visited, pq, currentNode, currentNode.Node->SouthGridCenter, end);
		}
		if (!currentNode.Node->EastEdge->IsBlocked())
		{
			CheckChildNode(visited, pq, currentNode, currentNode.Node->EastGridCenter, end);
		}
		if (!currentNode.Node->WestEdge->IsBlocked())
		{
			CheckChildNode(visited, pq, currentNode, currentNode.Node->WestGridCenter, end);
		}
	}

	if (foundEnd)
	{
		PuzzleVisitedNode backTrackNode = visited.at(currentNode.Node->GridPos);
		while (backTrackNode.Node != start)
		{
			path.Add(backTrackNode.Node);
			backTrackNode = visited.at(backTrackNode.CameFrom);
		}
	}
	return path;
}
}