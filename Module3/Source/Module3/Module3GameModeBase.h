// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Module3GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MODULE3_API AModule3GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
