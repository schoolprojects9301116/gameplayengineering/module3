// Fill out your copyright notice in the Description page of Project Settings.


#include "PuzzleGameMode.h"
#include "MouseBasedPlayerController.h"
#include "Monster.h"
#include "Consumable.h"
#include "PuzzleGridCenter.h"
#include "Kismet/GameplayStatics.h"
#include "PathfindingAlgorithms.h"

APuzzleGameMode::APuzzleGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
	DefaultPawnClass = AMouseBasedPlayerController::StaticClass();
}

void APuzzleGameMode::AddAction(CommandAction* action)
{
	CommandActionManager.AddAction(action);
	NotifyMonsters();
}

void APuzzleGameMode::UndoAction()
{
	if (CommandActionManager.UndoAction())
	{
		NotifyMonsters();
	}
}

void APuzzleGameMode::RedoAction()
{
	if (CommandActionManager.RedoAction())
	{
		NotifyMonsters();
	}
}

void APuzzleGameMode::NotifyMonsters()
{
	/*TSubclassOf<AMonster> monsterClassType;
	TArray<AActor*> foundMonsters;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), monsterClassType, foundMonsters);*/
	for (auto& monster : Monsters)
	{
		//auto monster = Cast<AMonster>(actor);
		UConsumable* closestConsumable = nullptr;
		APuzzleGridCenter* closestMove = nullptr;
		int closestConsumableDistance = -1;
		for (auto& consumable : Consumables)
		{
			auto path = PathfindingAlgorithms::PuzzleGameAStar(monster->CurrentGridPos, consumable->GridCenter);
			auto distance = path.Num();
			if (closestConsumable == nullptr && distance > 0)
			{
				closestConsumable = consumable;
				closestConsumableDistance = distance;
				closestMove = path.Last();
			}
			else if (distance > 0 && distance < closestConsumableDistance)
			{
				closestConsumable = closestConsumable;
				closestConsumableDistance = distance;
				closestMove = path.Last();
			}
		}
		if (closestMove != nullptr)
		{
			monster->MakeMove(closestMove);
		}
	}
}

int APuzzleGameMode::ManhattanDistance(std::tuple<int, int> a, std::tuple<int, int> b)
{
	return abs(std::get<0>(b) - std::get<0>(a)) + abs(std::get<1>(b) - std::get<1>(a));
}

void APuzzleGameMode::StartPlay()
{
	Super::StartPlay();
	_updateTimer = _updateInterval;
}

void APuzzleGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	_updateTimer -= DeltaTime;
	if (_updateTimer <= 0.f)
	{
		_updateTimer = _updateInterval;
		if (NumTargets <= 0)
		{
			// BREAK
			_levelComplete = true;
		}
	}
}